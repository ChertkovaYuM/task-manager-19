package ru.tsc.chertkova.tm.api.service;

import ru.tsc.chertkova.tm.enumerated.Status;
import ru.tsc.chertkova.tm.model.Task;

import java.util.Date;
import java.util.List;

public interface ITaskService extends IService<Task> {

    Task create(String name);

    Task create(String name, String description);

    Task create(String name, String description, Date dateBegin, Date dateEnd);

    List<Task> findAllByProjectId(String projectId);

    Task updateById(String id, String name, String description);

    Task changeTaskStatusById(String id, Status status);

}
