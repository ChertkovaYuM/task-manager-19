package ru.tsc.chertkova.tm.api.repository;

import ru.tsc.chertkova.tm.enumerated.Sort;
import ru.tsc.chertkova.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    M add(M model);

    void clear();

    List<M> findAll();

    boolean existsById(String id);

    M findById(String id);

    int getSize();

    M remove(M model);

    M removeById(String id);

    List<M> findAll(Comparator comparator);

    List<M> findAll(Sort sort);

}
