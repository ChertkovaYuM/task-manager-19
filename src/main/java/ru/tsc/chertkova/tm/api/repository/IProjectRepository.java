package ru.tsc.chertkova.tm.api.repository;

import ru.tsc.chertkova.tm.model.Project;

public interface IProjectRepository extends IRepository<Project> {

    Project create(String name);

    Project create(String name, String description);

}
