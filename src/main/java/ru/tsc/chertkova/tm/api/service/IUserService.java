package ru.tsc.chertkova.tm.api.service;

import ru.tsc.chertkova.tm.enumerated.RoleType;
import ru.tsc.chertkova.tm.model.User;

import java.util.List;

public interface IUserService extends IService<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, RoleType roleType);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeByLogin(String login);

    boolean isLoginExists(String login);

    boolean isEmailExists(String email);

    User setPassword(String userId, String password);

    User updateUser(String userId, String firstName, String lastName, String middleName);

}
