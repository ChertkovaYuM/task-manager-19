package ru.tsc.chertkova.tm.command.user;

import ru.tsc.chertkova.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    public static final String NAME = "user-login";

    public static final String DESCRIPTION = "Login user profile.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        getAuthService().login(login, password);
    }

}
